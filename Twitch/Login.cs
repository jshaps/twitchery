﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Windows.Forms;
using System.Xml;
using Twitchery;

namespace Twitch
{
    public partial class frmLogin : Form
    {
        private readonly string _configPath;

        public frmLogin(string configPath)
        {
            this._configPath = configPath;
            InitializeComponent();
            GetAuthorisation();
        }

        private void GetAuthorisation()
        {
                const string clientId = "uugmljae9zgvxd4sl7jik7gvz611fh";
                const string redirectURI = "http://localhost";
                const string responseType = "token";
                const string scope = "chat_login+user_read";

                Uri authorisationUrl =
                    new Uri(
                        $"https://api.twitch.tv/kraken/oauth2/authorize?client_id={clientId}&redirect_uri={redirectURI}&response_type={responseType}&scope={scope}");

                webLogin.Url = authorisationUrl;
                webLogin.Visible = true;
        }

        private void webLogin_Navigated(object sender, WebBrowserNavigatedEventArgs e)
        {
            var returnUrl = e.Url.ToString();

            if (returnUrl.Contains("localhost"))
            {

                if (returnUrl.StartsWith("http://localhost/#access_token="))
                {
                    string[] tokenStrings = returnUrl.Split('=');

                    var oauth = $"{tokenStrings[1].Split('&')[0]}";

                    try
                    {
                        using (XmlWriter xmlWriter = XmlWriter.Create(_configPath))
                        {
                            xmlWriter.WriteStartDocument();
                            xmlWriter.WriteStartElement("Config");

                            xmlWriter.WriteStartElement("UserDetails");

                            xmlWriter.WriteElementString("Oauth", oauth);

                            xmlWriter.WriteEndElement();

                            xmlWriter.WriteEndElement();
                            xmlWriter.WriteEndDocument();
                        }
                    }
                    catch (Exception exception)
                    {
                        Console.WriteLine(exception);
                        throw;
                    }

                    Authentication.Oauth = oauth;
                    Close();
                }
                else
                {
                    webLogin.Visible = false;
                    var dialogResult = MessageBox.Show(
                        "To use this application you must authorise access to your twitch account. \r\n Would you like to try again?",
                        "Application Authorisation", MessageBoxButtons.YesNo);
                    switch (dialogResult)
                    {
                        case DialogResult.Yes:
                            GetAuthorisation();
                            break;
                        case DialogResult.No:
                            Application.Exit();
                            break;
                    }
                }
            }
            else
            {
                GetAuthorisation();
            }
        }
    }
}
