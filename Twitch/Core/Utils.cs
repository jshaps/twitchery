﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Twitch.Core
{
    class Utils
    {
        public static string Capitalisation(string message)
        {
            if (string.IsNullOrEmpty(message))
                return string.Empty;

            char[] a = message.ToCharArray();
            a[0] = char.ToUpper(a[0]);
            return new string(a);
        }
    }
}
