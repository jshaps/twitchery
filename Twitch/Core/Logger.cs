﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Twitch
{
    class Logger
    {
        private const string OutputFile = "Message_Output.txt";

        public void Input(string message)
        {
            message = $"{DateTime.Now:dd/MM/yyyy hh:mm:ss tt} - {message}\r\n";
            if (!File.Exists(OutputFile))
            {
                File.WriteAllText(OutputFile, message, Encoding.UTF8);
            }
            else
            {
                
                File.AppendAllText(OutputFile, message, Encoding.UTF8);
            }
        }
        
    }
}
