﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Twitch
{
    class MessageHandler
    {
        public const string PrivMsg = "PRIVMSG";
        private const string ClearChat = "CLEARCHAT";
        private const string Ping = "PING";
        private const string Join = "JOIN";
        private const string Part = "PART";
        private const string UserState = "USERSTATE";
        private const string UserList = "353";
        private const string Mode = "MODE";
        private const string Notice = "NOTICE";

        public class ParsedMessage
        {
            public string Message { get; set; }
            public string Tags { get; set; }
            public string Command { get; set; }
            public string Channel { get; set; }
            public string Server { get; set; }
            public string Username { get; set; }
        }

        public ParsedMessage ParseMessage(string rawMessage)
        {
            ParsedMessage parsedMessage = new ParsedMessage();

            if (rawMessage[0] == ':')
            {
                rawMessage = $" {rawMessage}";
            }
            else if (rawMessage.StartsWith("PING"))
            {
                parsedMessage.Command = "PING";
                return parsedMessage;
            }

            var tagIndex = rawMessage.IndexOf(' ');
            parsedMessage.Tags = rawMessage.Substring(0, tagIndex);

            var serverIndex = rawMessage.IndexOf(' ', tagIndex + 1);

            parsedMessage.Server = rawMessage.Substring(tagIndex + 2, (serverIndex - tagIndex - 2));

            var commandIndex = rawMessage.IndexOf(' ', serverIndex + 1);
            parsedMessage.Command = rawMessage.Substring(serverIndex + 1, commandIndex - serverIndex - 1);

            int channelIndex;
            if (parsedMessage.Command != "353")
            {
                channelIndex = (rawMessage.IndexOf(' ', commandIndex + 1) == -1) ? rawMessage.Length
                    : rawMessage.IndexOf(' ', commandIndex + 1);
                parsedMessage.Channel = rawMessage.Substring(commandIndex + 1, channelIndex - commandIndex - 1);
            }
            else
            {
                channelIndex = rawMessage.IndexOf('#') + 1 ;
                parsedMessage.Channel = rawMessage.Substring(channelIndex, rawMessage.IndexOf(':', channelIndex + 1) - channelIndex - 1);
            }


            if (parsedMessage.Command == PrivMsg || parsedMessage.Command == "353" || parsedMessage.Command == Notice)
            {
                var messageIndex = rawMessage.IndexOf(':', channelIndex + 1);
                parsedMessage.Message = rawMessage.Substring(messageIndex + 1);

            } else if (parsedMessage.Command == Mode)
            {
//                ":jtv MODE #clintstevens +o xenoda"
                var messageIndex = rawMessage.IndexOf(' ', channelIndex);
                parsedMessage.Message = rawMessage.Substring(messageIndex + 1, 2);

//                var usernameIndex = rawMessage.IndexOf(' ', channelIndex + 2);
                var usernameIndex = messageIndex + 4;
                parsedMessage.Username = rawMessage.Substring(usernameIndex, rawMessage.Length - usernameIndex);
            } 

            if (parsedMessage.Server.Contains('!'))
            {
                parsedMessage.Username = rawMessage.Substring(tagIndex + 2, rawMessage.IndexOf('!') - tagIndex - 2);
            }

            return parsedMessage;
        }

        public void Recieved(frmTwitchClient frmTwitchClient, string rawMessage)
        {
            ChatCommand chatCommand = new ChatCommand();
            var message = ParseMessage(rawMessage);

            switch (message.Command)
            {
                case Ping:
                    chatCommand.Ping(frmTwitchClient, rawMessage);
                    break;
                case Join:
                    chatCommand.Join(frmTwitchClient, message.Channel, message.Username);
                    break;
                case Part:
                    chatCommand.Part(frmTwitchClient, message.Channel, message.Username);
                    break;
                case PrivMsg:
                    chatCommand.PrivMsg(frmTwitchClient, message.Channel, message.Username, message.Message);
                    break;
                case Notice:
                    chatCommand.Notice(frmTwitchClient, message.Channel, message.Username, message.Message);
                    break;
                case UserList:
                    string[] users = message.Message.Split(' ');
                    foreach (string user in users)
                    {
                        chatCommand.Join(frmTwitchClient, message.Channel, user);
                    }
                    break;
                case Mode:
                    chatCommand.Mode(frmTwitchClient, message.Channel, message.Username, message.Message);
                    break;
                default:
                    chatCommand.Debug(frmTwitchClient, rawMessage);
                    break;
            }
        }
    }
}
