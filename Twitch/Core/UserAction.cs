﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Windows.Forms;
using Twitchery;

namespace Twitch.Core
{
    class UserAction
    {
        public void SendMessage(frmTwitchClient frmTwitchClient, string channelName, string message)
        {
            Control[] controls = frmTwitchClient.Controls.Find("txt" + channelName + "Chat", true);
            TextBox chatTextBox = controls[0] as TextBox;

            chatTextBox.Text += $"\r\n{DateTime.Now.ToString("hh:mm:ss tt")} - {frmTwitchClient._userName}: {message}";
            

            var messagePrefix =
                $":{frmTwitchClient._userName}!{frmTwitchClient._userName}@{frmTwitchClient._userName}.tmi.twitch.tv {MessageHandler.PrivMsg} #{channelName} :";

            frmTwitchClient.Writer.WriteLine($"{messagePrefix}{message}");
            frmTwitchClient.Writer.Flush();
        }
        
        public void JoinChannel(frmTwitchClient frmTwitchClient, string channelName)
        {
            Channel channelList = new Channel
            {
                Name = channelName,
                BotEnabled = false
            };

            frmTwitchClient.Channels.Add(channelList);

            if (channelName[0] == '#')
            {
                channelName = channelName.Substring(1);
            }

            var channel = "#" + channelName;

            TabPage channelPage = new TabPage
            {
                Name = "tab" + channelName,
                Text = channel
            };

            TextBox channelTextBox = new TextBox
            {
                Name = "txt" + channelName + "Chat",
                Anchor = (AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right),
                Dock = DockStyle.Fill,
                Multiline = true,
                ReadOnly = true,
                ScrollBars = ScrollBars.Vertical,
                Size = new Size(697, 431),
            };

            channelPage.Controls.Add(channelTextBox);

            TextBox textToSend = new TextBox
            {
                Name = "txt" + channelName + "Message",
                AcceptsReturn = true,
                Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right,
                Dock = DockStyle.Bottom,
                Size = new Size(693, 20),
                Location = new Point(3, 435),
            };

            textToSend.KeyDown += (sender, e) => { txtSendToChat_KeyDown(frmTwitchClient, sender, e); };

            channelPage.Controls.Add(textToSend);

            ListBox userListBox = new ListBox
            {
                Name = "lst" + channelName + "Users",
                Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Right,
                Dock = DockStyle.Right,
                FormattingEnabled = true,
                Location = new Point(706, 3),
                Sorted = true,
                Size = new Size(197, 452)
            };
            
            channelPage.Controls.Add(userListBox);

            frmTwitchClient.tabTwitchBot.TabPages.Add(channelPage);

            frmTwitchClient.Writer.WriteLine($"JOIN {channel}");
            frmTwitchClient.Writer.Flush();
        }

        public void PartChannel(frmTwitchClient frmTwitchClient, string channelName)
        {
            frmTwitchClient.Writer.WriteLine($"PART #{channelName}");
            frmTwitchClient.Writer.Flush();

            frmTwitchClient.tabTwitchBot.TabPages.Remove(frmTwitchClient.tabTwitchBot.SelectedTab);
        }

        private void txtSendToChat_KeyDown(frmTwitchClient frmTwitchClient, object sender, KeyEventArgs e)
        {
            if (e.KeyCode != Keys.Enter)
                return;

            try
            {

                TextBox txtSender = sender as TextBox;

                var channel = txtSender.Name.Substring(3, (txtSender.Name.Length - 10));

                SendMessage(frmTwitchClient, channel, txtSender.Text);

                txtSender.Text = "";

                e.Handled = true;
                e.SuppressKeyPress = true;
            }
            catch (Exception errException)
            {
                Console.WriteLine(errException);
            }
        }
    }
}