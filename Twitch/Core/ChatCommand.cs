﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Twitch
{
    class ChatCommand
    {
        public void Join(frmTwitchClient frmTwitchClient, string channel, string username)
        {
            try
            {
                Control[] controls = frmTwitchClient.Controls.Find("lst" + channel.Substring(1) + "Users", true);
                ListBox userList = controls[0] as ListBox;

                if (userList != null && !userList.Items.Contains(username))
                {
                    userList.Items.Add(username);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            

            
        }

        public void Part(frmTwitchClient frmTwitchClient,string channel, string username)
        {
            if (username != frmTwitchClient._userName)
            {
                Control[] controls = frmTwitchClient.Controls.Find("lst" + channel.Substring(1) + "Users", true);
                ListBox userList = controls[0] as ListBox;

                userList.Items.Remove(username);
            }
        }

        public void Ping(frmTwitchClient frmTwitchClient, string message)
        {
            frmTwitchClient.txtQuery.Text += $"\r\n{message}";
            frmTwitchClient.Writer.WriteLine("PONG :tmi.twitch.tv");
            frmTwitchClient.Writer.Flush();

            frmTwitchClient.txtQuery.Text += $"\r\nPONG :tmi.twitch.tv";
        }

        public void PrivMsg(frmTwitchClient frmTwitchClient,string channel, string username, string message)
        {
            Channel channelDetails = frmTwitchClient.Channels.Find(x => x.Name == channel.Substring(1));
            Control[] controls = frmTwitchClient.Controls.Find("txt" + channel.Substring(1) + "Chat", true);
            TextBox textBox = controls[0] as TextBox;

           textBox.Text += $"\r\n{DateTime.Now.ToString("hh:mm:ss tt")} - {username}: {message}";

            if (message[0] == '!'&& channelDetails.BotEnabled)
            {
                Bot bot = new Bot();

                bot.Start(frmTwitchClient, channel, username, message);
            }


            textBox.SelectionStart = textBox.TextLength;
            textBox.ScrollToCaret();

        }

        public void Notice(frmTwitchClient frmTwitchClient, string channel, string username, string message)
        {
            Control[] controls = frmTwitchClient.Controls.Find("txt" + channel.Substring(1) + "Chat", true);
            TextBox textBox = controls[0] as TextBox;

            textBox.Text += $"\r\n{DateTime.Now.ToString("hh:mm:ss tt")} -----NOTICE---- {message}";
        }

        public void Mode(frmTwitchClient frmTwitchClient, string channel, string username, string message)
        {
            try
            {
                if (message[0] == '+')
                {
                    switch (message[1])
                    {
                        case 'o':
                            //TODO Set username as moderator
                            Control[] controls = frmTwitchClient.Controls.Find("lst" + channel.Substring(1) + "Users", true);
                            ListBox userList = controls[0] as ListBox;

                            if (userList != null) userList.Items[userList.FindStringExact(username)] = $"@{username}";

                            userList.Sorted = true;
                            break;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            
        }
        public void Debug(frmTwitchClient frmTwitchClient, string message)
        {
            frmTwitchClient.txtQuery.Text += $"\r\n{message}";
        }
    }
}
