﻿namespace Twitch
{
    partial class frmLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.webLogin = new System.Windows.Forms.WebBrowser();
            this.SuspendLayout();
            // 
            // webLogin
            // 
            this.webLogin.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webLogin.Location = new System.Drawing.Point(0, 0);
            this.webLogin.MinimumSize = new System.Drawing.Size(20, 20);
            this.webLogin.Name = "webLogin";
            this.webLogin.ScrollBarsEnabled = false;
            this.webLogin.Size = new System.Drawing.Size(718, 587);
            this.webLogin.TabIndex = 0;
            this.webLogin.Url = new System.Uri("", System.UriKind.Relative);
            this.webLogin.Navigated += new System.Windows.Forms.WebBrowserNavigatedEventHandler(this.webLogin_Navigated);
            // 
            // frmLogin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(718, 587);
            this.Controls.Add(this.webLogin);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmLogin";
            this.Text = "Login";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.WebBrowser webLogin;
    }
}