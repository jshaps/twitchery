﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Twitch
{
    internal enum UserType
    {
        empty, mod, global_mod, admin, staff
    }
}
