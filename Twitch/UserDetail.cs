﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Twitch
{
    [DataContract(Name = "user")]
    class UserDetail
    {
        [DataMember(Name = "_id")]
        public int Id { get; set; }

        [DataMember(Name = "bio")]
        public string Bio { get; set; }

        [DataMember(Name = "created_at")]
        public DateTime CreatedAt { get; set; }

        [DataMember(Name = "display_name")]
        public string DisplayName { get; set; }

        [DataMember(Name = "email")]
        public string Email { get; set; }

        [DataMember(Name = "email_verified")]
        public bool EmailVerified { get; set; }

        [DataMember(Name = "logo")]
        public string Logo { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "partnered")]
        public bool Partnered { get; set; }

        [DataMember(Name = "twitter_conected")]
        public bool TwitterConnected { get; set; }

        [DataMember(Name = "type")]
        public string UserType { get; set; }

        [DataMember(Name = "updated_at")]
        public DateTime UpdatedAt { get; set; }

    }
}
