﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Twitch;

namespace Twitchery
{
    class User
    {
        //:vallis_ukiyo!vallis_ukiyo@vallis_ukiyo.tmi.twitch.tv JOIN #floppy_pancakes
        /*@badges=premium/1;
        color=#FF69B4;
        display-name=cattzs;
        emotes=118149:64-75,77-88,90-101/149232:103-113,115-125/148227:127-137,139-149/107524:151-158,160-167;
        id=92fc4c7f-ea37-4dfd-9a61-59a7c3d7eb23;
        mod=0;
        room-id=109351872;
        sent-ts=1496285249847;
        subscriber=0;
        tmi-sent-ts=1496285259451;
        turbo=0;
        user-id=43370771;
        user-type= :cattzs!cattzs@cattzs.tmi.twitch.tv
        */
        public string Name { get; set; }
        public string Color { get; set; }
        public string DisplayName { get; set; }
        public List<string> EmotesList { get; set; }
        public bool Moderator { get; set; }
        public bool Subscriber { get; set; }
        public bool TwitchTurbo { get; set; }
        public UserType TwitchUserType { get; set; }

    }
}
