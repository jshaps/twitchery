﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Forms;
using Timer = System.Timers.Timer;

namespace Twitch.Pig_Race
{
    public partial class frmPigRace : Form
    {
        private int _raceCountDown;
        private int _raceSectionSize;
        private int _currentSection;
        private int _winningPoint;

        private int? _winner;
        private bool clearScreen;

        public frmPigRace()
        {
            InitializeComponent();
        }

        private void frmPigRace_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            _raceSectionSize = (this.Width - 24 - imgPigOne.Width) / 4;

            
            _winningPoint = this.Width - 40 - imgPigOne.Width;

        }

        private void StartPigRace()
        {
            _winner = null;
            imgPigOne.Location = new Point(12, 480);
            imgPigOne.Visible = true;

            imgPigTwo.Location = new Point(12, 245);
            imgPigTwo.Visible = true;

            //Count down
            _raceCountDown = 5;
            tmrRaceStart.Start();

        }
        private void tmrRaceStart_Tick(object sender, EventArgs e)
        {
            if (_raceCountDown > 0)
            {
                txtCountDown.Visible = true;
                txtCountDown.Text = _raceCountDown.ToString();
                _raceCountDown = _raceCountDown - 1;
            } else if (_raceCountDown == 0)
            {
                txtCountDown.Text = $"GO!";
                _raceCountDown = _raceCountDown - 1;
                PigRace();
            }
            else
            {
                txtCountDown.Visible = false;
                tmrRaceStart.Stop();
            }
        }

        private void PigRace()
        {
            var raceSections = 4;

            SetPigOneSpeed();
            SetPigTwoSpeed();
            tmrPigOne.Start();
            tmrPigTwo.Start();

        }

        private void SetPigOneSpeed()
        {
            Random random = new Random();

            var pigOneSpeed = random.Next(1, 100);

            //Update interval
            tmrPigOne.Interval = pigOneSpeed;
        }

        private void SetPigTwoSpeed()
        {
            Random random = new Random();

            var pigSpeed = random.Next(1, 100);

            //Update interval
            tmrPigTwo.Interval = pigSpeed;
        }

        private void tmrPigOne_Tick(object sender, EventArgs e)
        {
            
            var currentLocation = imgPigOne.Location.X;

            if (currentLocation < _currentSection * _raceSectionSize && _winner == null)
            {
                currentLocation += 3;
                imgPigOne.Location = new Point(currentLocation, 480);

                if (currentLocation >= _winningPoint)
                {
                    tmrPigOne.Stop();
                    tmrPigTwo.Stop();
                    clearScreen = false;
                    tmrClearScreen.Start();
                }
            }
            else if (_currentSection <= 4 && _winner == null)
            {
                _currentSection++;
                SetPigOneSpeed();
            }
            else
            {

                tmrPigOne.Stop();
            }
        }

        private void tmrPigTwo_Tick(object sender, EventArgs e)
        {
            var currentLocation = imgPigTwo.Location.X;

            if (currentLocation < _currentSection * _raceSectionSize && _winner == null)
            {
                currentLocation += 3;
                imgPigTwo.Location = new Point(currentLocation, 245);

                if (currentLocation >= _winningPoint)
                {
                    tmrPigOne.Stop();
                    tmrPigTwo.Stop();

                    clearScreen = false;
                    tmrClearScreen.Start();
                }
            }
            else if (_currentSection <= 4 && _winner == null)
            {
                _currentSection++;
                SetPigTwoSpeed();
            }
            else
            {

                tmrPigTwo.Stop();
            }
        }


        private void frmPigRace_MouseClick(object sender, MouseEventArgs e)
        {
            StartPigRace();
        }

        private void tmrClearScreen_Tick(object sender, EventArgs e)
        {
            if (clearScreen)
            {
                imgPigOne.Visible = false;
                imgPigTwo.Visible = false;
                tmrClearScreen.Stop();
            }
            clearScreen = true;
        }
    }
}
