﻿namespace Twitch.Pig_Race
{
    partial class frmPigRace
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.imgPigOne = new System.Windows.Forms.PictureBox();
            this.txtCountDown = new System.Windows.Forms.TextBox();
            this.tmrRaceStart = new System.Windows.Forms.Timer(this.components);
            this.tmrPigOne = new System.Windows.Forms.Timer(this.components);
            this.tmrPigTwo = new System.Windows.Forms.Timer(this.components);
            this.imgPigTwo = new System.Windows.Forms.PictureBox();
            this.tmrClearScreen = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.imgPigOne)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgPigTwo)).BeginInit();
            this.SuspendLayout();
            // 
            // imgPigOne
            // 
            this.imgPigOne.BackColor = System.Drawing.Color.Lime;
            this.imgPigOne.Cursor = System.Windows.Forms.Cursors.Default;
            this.imgPigOne.Image = global::Twitch.Properties.Resources.pig1;
            this.imgPigOne.Location = new System.Drawing.Point(12, 480);
            this.imgPigOne.Name = "imgPigOne";
            this.imgPigOne.Size = new System.Drawing.Size(178, 134);
            this.imgPigOne.TabIndex = 0;
            this.imgPigOne.TabStop = false;
            // 
            // txtCountDown
            // 
            this.txtCountDown.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtCountDown.BackColor = System.Drawing.Color.Lime;
            this.txtCountDown.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCountDown.Font = new System.Drawing.Font("Luckiest Guy", 140F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.txtCountDown.Location = new System.Drawing.Point(500, 136);
            this.txtCountDown.Name = "txtCountDown";
            this.txtCountDown.ReadOnly = true;
            this.txtCountDown.Size = new System.Drawing.Size(348, 229);
            this.txtCountDown.TabIndex = 1;
            this.txtCountDown.Text = "5";
            this.txtCountDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtCountDown.Visible = false;
            // 
            // tmrRaceStart
            // 
            this.tmrRaceStart.Interval = 1000;
            this.tmrRaceStart.Tick += new System.EventHandler(this.tmrRaceStart_Tick);
            // 
            // tmrPigOne
            // 
            this.tmrPigOne.Tick += new System.EventHandler(this.tmrPigOne_Tick);
            // 
            // tmrPigTwo
            // 
            this.tmrPigTwo.Tick += new System.EventHandler(this.tmrPigTwo_Tick);
            // 
            // imgPigTwo
            // 
            this.imgPigTwo.BackColor = System.Drawing.Color.Lime;
            this.imgPigTwo.Cursor = System.Windows.Forms.Cursors.Default;
            this.imgPigTwo.Image = global::Twitch.Properties.Resources.pig1;
            this.imgPigTwo.Location = new System.Drawing.Point(12, 245);
            this.imgPigTwo.Name = "imgPigTwo";
            this.imgPigTwo.Size = new System.Drawing.Size(178, 134);
            this.imgPigTwo.TabIndex = 2;
            this.imgPigTwo.TabStop = false;
            // 
            // tmrClearScreen
            // 
            this.tmrClearScreen.Interval = 2250;
            this.tmrClearScreen.Tick += new System.EventHandler(this.tmrClearScreen_Tick);
            // 
            // frmPigRace
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Lime;
            this.ClientSize = new System.Drawing.Size(1330, 626);
            this.Controls.Add(this.imgPigTwo);
            this.Controls.Add(this.txtCountDown);
            this.Controls.Add(this.imgPigOne);
            this.Name = "frmPigRace";
            this.Text = "Pig_Race";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmPigRace_Load);
            this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.frmPigRace_MouseClick);
            ((System.ComponentModel.ISupportInitialize)(this.imgPigOne)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgPigTwo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox imgPigOne;
        private System.Windows.Forms.TextBox txtCountDown;
        private System.Windows.Forms.Timer tmrRaceStart;
        private System.Windows.Forms.Timer tmrPigOne;
        private System.Windows.Forms.Timer tmrPigTwo;
        private System.Windows.Forms.PictureBox imgPigTwo;
        private System.Windows.Forms.Timer tmrClearScreen;
    }
}