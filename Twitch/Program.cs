﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using Twitch;

namespace Twitchery
{
    static class Program
    {
        private const string ConfigPath = "config.xml";

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            GetLoginDetails();
            Application.Run(new frmTwitchClient());
        }

        private static void GetLoginDetails()
        {
            if (!File.Exists(ConfigPath))
            {
                Application.Run(new frmLogin(ConfigPath));
            }
            else
            {
                using (XmlReader reader = XmlReader.Create(ConfigPath))
                {
                    while (reader.Read())
                    {
                        if (reader.IsStartElement() && reader.Name == "Oauth")
                        {
                            if (reader.Read())
                            {
                                Authentication.Oauth = reader.Value;
                            }
                        }
                    }
                }
            }
        }
    }
}
