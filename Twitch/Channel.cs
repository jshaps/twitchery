﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Twitch
{
    public class Channel
    {
        public string Name { get; set; }
        public bool BotEnabled { get; set; }
    }
}
