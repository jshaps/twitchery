﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Twitch.Core;

namespace Twitch
{
    class Bot
    {
//        readonly frmTwitchClient _frmTwitchClient = new frmTwitchClient();

        public void Start(frmTwitchClient frmTwitchClient, string channelName, string username, string message)
        {
            if (channelName[0] == '#')
                channelName = channelName.Substring(1);
            

            UserAction userAction = new UserAction();

            string botReturn = null;

            if (message.ToUpper() == "!SLOT")
            {
                botReturn = Slot(username);

            } else if (message.ToUpper().StartsWith("!CROC"))
            {
                try
                {
                    var victim = message.Split(' ')[1];

                    botReturn = $"{frmTwitchClient._userName} fed {victim} to a big ass crocodile.";

                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }

            }

            userAction.SendMessage(frmTwitchClient, channelName, botReturn);
        }

        public string Slot(string username)
        {
            string[] slot = new string[3];
            string[] slotCharacters = new[] {"Kappa", "DansGame", "Keepo", "BrokeBack", "SwiftRage"};
            Random random = new Random();

            for (int i = 0; i < 3; i++)
            {
                int randomNumber = random.Next(0, 4);

                slot[i] = slotCharacters[randomNumber];
            }

            return $"{username} spun {slot[0]} + {slot[1]} + {slot[2]}";
        }
    }
}
