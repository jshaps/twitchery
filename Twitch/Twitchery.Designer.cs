﻿namespace Twitch
{
    partial class frmTwitchClient
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.mnuTopBar = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuTopBarChannel = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuTopBarChannelJoin = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuTopBarChannelPart = new System.Windows.Forms.ToolStripMenuItem();
            this.hostToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabDebug = new System.Windows.Forms.TabPage();
            this.txtDebug = new System.Windows.Forms.TextBox();
            this.tabQuery = new System.Windows.Forms.TabPage();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.lstUserNames = new System.Windows.Forms.ListBox();
            this.txtQuery = new System.Windows.Forms.TextBox();
            this.tabTwitchBot = new System.Windows.Forms.TabControl();
            this.tmrChatText = new System.Windows.Forms.Timer(this.components);
            this.mnuTopBar.SuspendLayout();
            this.tabDebug.SuspendLayout();
            this.tabQuery.SuspendLayout();
            this.tabTwitchBot.SuspendLayout();
            this.SuspendLayout();
            // 
            // mnuTopBar
            // 
            this.mnuTopBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem,
            this.mnuTopBarChannel});
            this.mnuTopBar.Location = new System.Drawing.Point(0, 0);
            this.mnuTopBar.Name = "mnuTopBar";
            this.mnuTopBar.Size = new System.Drawing.Size(914, 24);
            this.mnuTopBar.TabIndex = 1;
            this.mnuTopBar.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.editToolStripMenuItem.Text = "Edit";
            // 
            // mnuTopBarChannel
            // 
            this.mnuTopBarChannel.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuTopBarChannelJoin,
            this.mnuTopBarChannelPart,
            this.hostToolStripMenuItem});
            this.mnuTopBarChannel.Name = "mnuTopBarChannel";
            this.mnuTopBarChannel.Size = new System.Drawing.Size(63, 20);
            this.mnuTopBarChannel.Text = "&Channel";
            // 
            // mnuTopBarChannelJoin
            // 
            this.mnuTopBarChannelJoin.Name = "mnuTopBarChannelJoin";
            this.mnuTopBarChannelJoin.Size = new System.Drawing.Size(99, 22);
            this.mnuTopBarChannelJoin.Text = "Join";
            this.mnuTopBarChannelJoin.Click += new System.EventHandler(this.mnuTopBarChannelJoin_Click);
            // 
            // mnuTopBarChannelPart
            // 
            this.mnuTopBarChannelPart.Name = "mnuTopBarChannelPart";
            this.mnuTopBarChannelPart.Size = new System.Drawing.Size(99, 22);
            this.mnuTopBarChannelPart.Text = "Part";
            this.mnuTopBarChannelPart.Click += new System.EventHandler(this.mnuTopBarChannelPart_Click);
            // 
            // hostToolStripMenuItem
            // 
            this.hostToolStripMenuItem.Name = "hostToolStripMenuItem";
            this.hostToolStripMenuItem.Size = new System.Drawing.Size(99, 22);
            this.hostToolStripMenuItem.Text = "Host";
            this.hostToolStripMenuItem.Click += new System.EventHandler(this.hostToolStripMenuItem_Click);
            // 
            // tabDebug
            // 
            this.tabDebug.Controls.Add(this.txtDebug);
            this.tabDebug.Location = new System.Drawing.Point(4, 22);
            this.tabDebug.Name = "tabDebug";
            this.tabDebug.Size = new System.Drawing.Size(906, 458);
            this.tabDebug.TabIndex = 2;
            this.tabDebug.Text = "DEBUG";
            this.tabDebug.UseVisualStyleBackColor = true;
            // 
            // txtDebug
            // 
            this.txtDebug.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtDebug.Location = new System.Drawing.Point(0, 0);
            this.txtDebug.Multiline = true;
            this.txtDebug.Name = "txtDebug";
            this.txtDebug.ReadOnly = true;
            this.txtDebug.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtDebug.Size = new System.Drawing.Size(906, 458);
            this.txtDebug.TabIndex = 4;
            // 
            // tabQuery
            // 
            this.tabQuery.Controls.Add(this.textBox1);
            this.tabQuery.Controls.Add(this.lstUserNames);
            this.tabQuery.Controls.Add(this.txtQuery);
            this.tabQuery.Location = new System.Drawing.Point(4, 22);
            this.tabQuery.Name = "tabQuery";
            this.tabQuery.Padding = new System.Windows.Forms.Padding(3);
            this.tabQuery.Size = new System.Drawing.Size(906, 458);
            this.tabQuery.TabIndex = 0;
            this.tabQuery.Text = "Query";
            this.tabQuery.UseVisualStyleBackColor = true;
            // 
            // textBox1
            // 
            this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox1.Location = new System.Drawing.Point(3, 435);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(697, 20);
            this.textBox1.TabIndex = 6;
            // 
            // lstUserNames
            // 
            this.lstUserNames.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lstUserNames.FormattingEnabled = true;
            this.lstUserNames.Location = new System.Drawing.Point(706, 3);
            this.lstUserNames.Name = "lstUserNames";
            this.lstUserNames.Size = new System.Drawing.Size(197, 446);
            this.lstUserNames.TabIndex = 5;
            // 
            // txtQuery
            // 
            this.txtQuery.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtQuery.Location = new System.Drawing.Point(3, 3);
            this.txtQuery.MaxLength = 65534;
            this.txtQuery.Multiline = true;
            this.txtQuery.Name = "txtQuery";
            this.txtQuery.ReadOnly = true;
            this.txtQuery.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtQuery.Size = new System.Drawing.Size(697, 431);
            this.txtQuery.TabIndex = 4;
            // 
            // tabTwitchBot
            // 
            this.tabTwitchBot.Controls.Add(this.tabQuery);
            this.tabTwitchBot.Controls.Add(this.tabDebug);
            this.tabTwitchBot.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabTwitchBot.Location = new System.Drawing.Point(0, 24);
            this.tabTwitchBot.Name = "tabTwitchBot";
            this.tabTwitchBot.SelectedIndex = 0;
            this.tabTwitchBot.Size = new System.Drawing.Size(914, 484);
            this.tabTwitchBot.TabIndex = 0;
            this.tabTwitchBot.SelectedIndexChanged += new System.EventHandler(this.tabTwitchBot_SelectedIndexChanged);
            this.tabTwitchBot.TabIndexChanged += new System.EventHandler(this.Update);
            // 
            // tmrChatText
            // 
            this.tmrChatText.Enabled = true;
            this.tmrChatText.Tick += new System.EventHandler(this.Update);
            // 
            // frmTwitchClient
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(914, 508);
            this.Controls.Add(this.tabTwitchBot);
            this.Controls.Add(this.mnuTopBar);
            this.MainMenuStrip = this.mnuTopBar;
            this.Name = "frmTwitchClient";
            this.Text = "Twitch Bot";
            this.Load += new System.EventHandler(this.frmTwitchClient_Load);
            this.mnuTopBar.ResumeLayout(false);
            this.mnuTopBar.PerformLayout();
            this.tabDebug.ResumeLayout(false);
            this.tabDebug.PerformLayout();
            this.tabQuery.ResumeLayout(false);
            this.tabQuery.PerformLayout();
            this.tabTwitchBot.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.MenuStrip mnuTopBar;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mnuTopBarChannel;
        private System.Windows.Forms.ToolStripMenuItem mnuTopBarChannelJoin;
        private System.Windows.Forms.TabPage tabDebug;
        public System.Windows.Forms.TextBox txtDebug;
        private System.Windows.Forms.TabPage tabQuery;
        public System.Windows.Forms.ListBox lstUserNames;
        public System.Windows.Forms.TextBox txtQuery;
        public System.Windows.Forms.TabControl tabTwitchBot;
        private System.Windows.Forms.Timer tmrChatText;
        private System.Windows.Forms.ToolStripMenuItem hostToolStripMenuItem;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.ToolStripMenuItem mnuTopBarChannelPart;
    }
}

