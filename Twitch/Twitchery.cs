﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Sockets;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using System.Windows.Forms;
using Twitch.Core;
using Twitch.Pig_Race;
using Twitchery;

namespace Twitch
{
    public partial class frmTwitchClient : Form
    {
        readonly UserAction _userAction = new UserAction();
        private readonly Queue<string> _sendMessageQueue;
        public List<Channel> Channels = new List<Channel>();

        private TcpClient _tcpClient;
        private StreamReader _reader;
        public StreamWriter Writer;

        public readonly string _userName;
        public string _password;
        private readonly string _channelName;

        public readonly string _messagePrefix;
        private readonly string _privMsg;

        private DateTime lastMessage;
        private string selectedChannel;
//        public static string Channel;

        public frmTwitchClient()
        {

            _sendMessageQueue = new Queue<string>();

            this._userName = "null513".ToLower();

            this._privMsg = "PRIVMSG";
//            this._messagePrefix = $":{_userName}!{_userName}@{_userName}.tmi.twitch.tv {_privMsg} #{_channelName} :";

//            frmLogin frmLoginAuth = new frmLogin();

//            frmLoginAuth.ShowDialog();
        
            this._password = $"oauth:{Authentication.Oauth}";

            InitializeComponent();
            Reconnect();
        }

        private void Reconnect()
        {
            

            _tcpClient = new TcpClient("irc.chat.twitch.tv", 6667);
            _reader = new StreamReader(_tcpClient.GetStream());
            Writer = new StreamWriter(_tcpClient.GetStream());

            Writer.WriteLine("PASS " + _password + Environment.NewLine 
                + "NICK " + _userName + Environment.NewLine 
                + "USER " + _userName + " 8 *:" + _userName);

            Writer.WriteLine("CAP REQ :twitch.tv/membership");
            Writer.WriteLine("CAP REQ :twitch.tv/tags");
            Writer.WriteLine("CAP REQ :twitch.tv/commands");
//            userAction.JoinChannel(this, _channelName);
            //            writer.WriteLine($"JOIN #{_channelName}");
            Writer.Flush();

//            userAction.JoinChannel(this, _channelName);

            lastMessage = DateTime.Now;
        }

        private void Update(object sender, EventArgs e)
        {
            if (!_tcpClient.Connected)
            {
                Reconnect();
            }

            TrySendingMessages();

            TryRecieveMessages();
        }

        void TrySendingMessages()
        {
            if ((DateTime.Now - lastMessage) > TimeSpan.FromSeconds(2))
            {
                if (_sendMessageQueue.Count > 0)
                {
                    var message = _sendMessageQueue.Dequeue();
                    Writer.WriteLine($"{_messagePrefix}{message}");
                    Writer.Flush();
                    lastMessage = DateTime.Now;
                }
            }
        }

        private void TryRecieveMessages()
        {
            if (_tcpClient.Available > 0 || _reader.Peek() > 0)
            {
                MessageHandler messageHandler = new MessageHandler();
                Logger logger = new Logger();

                var message = _reader.ReadLine();

                Console.WriteLine(message);  //TODO Remove, used for debugging

                logger.Input(message);

                messageHandler.Recieved(this, message);

                //                TODO If the scrollbar is not at the bottom maintain position
//                txtChatText.SelectionStart = txtChatText.TextLength;
//                txtChatText.ScrollToCaret();
            }
        }


        public void SendMessage(string message)
        {
            _sendMessageQueue.Enqueue(message);
        }

        private void mnuTopBarChannelJoin_Click(object sender, EventArgs e)
        {
            var channel = InputDialog.ShowDialog("Channel", "Enter the stream you wish to join.").ToLower();
            _userAction.JoinChannel(this, channel);
        }

        private void mnuTopBarChannelPart_Click(object sender, EventArgs e)
        {
            _userAction.PartChannel(this, selectedChannel);
        }

        private void hostToolStripMenuItem_Click(object sender, EventArgs e)
        {
//            userAction.SendMessage(this, "wobblytrout");
frmPigRace frmPigRace = new frmPigRace();
            frmPigRace.Show();

        }



        public void tabTwitchBot_SelectedIndexChanged(object sender, EventArgs e)
        {
            TabControl tab = sender as TabControl;
            selectedChannel = tab.SelectedTab.Name.Substring(3);
        }

        private void frmTwitchClient_Load(object sender, EventArgs e)
        {

        }


    }
}
