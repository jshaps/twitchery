﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Twitch
{

    class Authentication
    {
        private static readonly HttpClient Client = new HttpClient();
        public static string Oauth { get; set; }

        public static async Task RunAsync()
        {
            AuthenticationHeaderValue oAuthentication = new AuthenticationHeaderValue("OAuth", Oauth);
            Client.BaseAddress = new Uri("https://api.twitch.tv/kraken/");
            Client.DefaultRequestHeaders.Accept.Clear();
            Client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/vnd.twitchtv.v5+json"));
            Client.DefaultRequestHeaders.Authorization = oAuthentication;

            var stringTask = Client.GetStringAsync($"{Client.BaseAddress}user");

            var msg = await stringTask;
            Console.Write(msg);
        }

        public static async Task<string> GetUserDetailsAsync(string path)
        {
            string userDetail = null;
            HttpResponseMessage response = await Client.GetAsync(path);

            if (response.IsSuccessStatusCode)
            {
//                userDetail = await response.Content.ReadAsAsync<UserDetail>();
                userDetail = await Client.GetStringAsync($"{Client.BaseAddress}user");
            }
            return userDetail;
        }
    }
}
